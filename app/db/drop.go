package db

import (
	"context"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/mongo"
)

func DropAllCollections(ctx context.Context, database *mongo.Database, logger *logrus.Logger) error {
	err := database.Collection("admin").Drop(ctx)
	if err != nil {
		logger.Error(err)
		return err
	} else {
		logger.Debugf("Collection: %s - dropped", "admins")
	}
	logger.Info("Dropping all collections completed")
	return nil
}
