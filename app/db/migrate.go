package db

import (
	"context"
	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/x/bsonx"
)

func MigrateDatabase(ctx context.Context, database *mongo.Database, logger *logrus.Logger) error {
	err := migrateIndexes(ctx, database, logger)
	if err != nil {
		logger.Error(err)
		return err
	}
	logger.Info("Migration completed")
	return nil
}

func migrateIndexes(ctx context.Context, database *mongo.Database, logger *logrus.Logger) error {
	emailIndex := false

	indexes, err := database.Collection("admin").Indexes().ListSpecifications(ctx)
	if err != nil {
		return err
	}

	for _, index := range indexes {
		switch index.Name {
		case "email":
			emailIndex = true
		}
	}

	if !emailIndex {
		err = createEmailIndex(ctx, database, logger)
		if err != nil {
			return err
		}
	}
	return nil
}

func createEmailIndex(ctx context.Context, database *mongo.Database, logger *logrus.Logger) error {
	keys := bsonx.Doc{
		{Key: "email", Value: bsonx.Int32(1)},
	}
	admin := database.Collection("admin")
	indexName, err := CreateUniqueIndex(ctx, admin, keys, "email")
	if err != nil {
		return err
	}
	logger.Debugf("Unique Index: %s created in the collection: %s", indexName, "admin")
	return nil
}
