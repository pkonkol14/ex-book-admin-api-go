Command line tools kubectx and kubensfootnote:[kubectx + kubens: Power tools for kubectl https://github.com/ahmetb/kubectx] are very helpful which switching clusters and namespaces.

Those tools are optional and can be installed with brew.

[source,bash]
----
brew install kubectx
----
